FROM node:18

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
# Copying source files
COPY . .

#Install dependencies
WORKDIR /usr/src/app/
RUN yarn install

EXPOSE 8080

# Running the app
CMD ["yarn", "start"]