// Express setup
const express = require("express");
const app = express();
app.use(express.json());


// Cors setup
const cors = require('cors');
//const corsOptions = { origin: 'http://localhost:3000' };  //can be replaced by NEXTAUTH_URL
const corsOptions = {};
app.use(cors(corsOptions));


// // Import your controllers here
// const userController = require('./controllers/userController')
// const authController = require('./controllers/resetPasswordController'); // Import your controller module
// const rolesController = require('./controllers/rolesController')
// const organizationManagementController = require('./controllers/organizationJobPostionsController')
// const departmentsAndJobsController = require('./controllers/departmentsAndJobsController')
// const employeeController = require('./controllers/employeeController')
// const salaryAndSupplementsController = require('./controllers/salaryAndSupplementsController')
// const payrollController = require('./controllers/payrollController')
//

// Home route
app.get("/", function (req, res) {
  res.status(200).json({ success: true, message: "Success" });
});


//
// // Reset password API
// app.post('/api/request-reset', authController);
// app.post('/api/reset-password', authController);
// app.post('/api/check-token', authController);
//
//
// // Terminate a row users/employees
// app.post("/api/update/:data_type/:id/:status", userController.terminateRow);
//
//
// // Roles management API
// app.get('/api/roles', rolesController.getAll);
//
//
// // User management API
// app.post("/api/register-user", userController.register);
// app.post("/api/edit/user/:user_id", userController.editUser);
// app.get("/api/usersOf/:organization_id/:role_id", userController.getAll);
// app.get("/api/users/admins", userController.getAllAdmins);
// app.get("/api/users/simpleUsers", userController.getAllSimpleUsers);
// app.get("/api/users/typeOf/:userType", userController.getUserTypeOf);
// app.get("/api/user", userController.login);
// app.get("/api/user/by_email/:email/role_id/:role_id", userController.getUserByEmail);
// app.get("/api/user/:user_id", userController.getCurrentUser);
//
//
// // Employee management API
// app.post("/api/register-employee", employeeController.addEmployee);
// app.post("/api/edit/employee/:employee_id", employeeController.editEmployee);
// app.post("/api/edit/employee-salary", employeeController.editEmployeeSalary);
// app.post("/api/addEmployeeSupplement", employeeController.addEmployeeSupplement);
// app.post("/api/editEmployeeSupplement", employeeController.editEmployeeSupplement);
//
// app.get("/api/employees/:organization_id", employeeController.getEmployeesByOrganization);
// app.get("/api/employee/:email", employeeController.getEmployeeByEmail);
//
//
// // Organization management API
// app.post('/api/registerOrganization', organizationManagementController.register);
// app.post("/api/editOrganization/:organization_id", organizationManagementController.editOrganization);
// app.get("/api/organizations/:user_id", organizationManagementController.getAllOrganizations);
// app.get("/api/organization/:organization", organizationManagementController.getOrganizationByName);
//
//
//
// // Departments management API
// app.post("/api/register-department", departmentsAndJobsController.addDepartment);
// app.post("/api/edit/department/:department_id", departmentsAndJobsController.editDepartment);
// app.get("/api/departments/:organization_id", departmentsAndJobsController.getAllDepartments);
// app.get("/api/department/:department_id", departmentsAndJobsController.getDepartmentById);
//
//
// // Jobs management API
// app.post("/api/register-job", departmentsAndJobsController.addJob);
// app.post("/api/edit/job/:job_id", departmentsAndJobsController.editJob);
// app.get("/api/jobs/:organization_id", departmentsAndJobsController.getAllJobs);
// app.get("/api/job/:job_id", departmentsAndJobsController.getJobById);
// app.get("/api/positions/:department_id", organizationManagementController.getAllPositions);
//
//
// // Employee history management API
// app.get("/api/salary-history/:employeeId", employeeController.getEmployeeSalaryHistory);
// app.post("/api/editEmployeeSalaryHistory", employeeController.editEmployeeSalaryHistory);
//
// // Salary and supplements management API
// app.get("/api/supplements", salaryAndSupplementsController.getAllSupplementsTypes);
//
//
// // Salary and  supplements management add API
// app.post("/api/addSupplement", salaryAndSupplementsController.addSupplement);
// app.post("/api/edit/supplement/:supplement_id", salaryAndSupplementsController.editSupplement);
// app.get("/api/supplement/:supplement_id", salaryAndSupplementsController.getSupplementById);
//
//
// // Payroll management API
// app.get("/api/payrolls/:organization_id", payrollController.getPayrolls);
// app.get("/api/payrollEmployees/:payroll_id", payrollController.getPayrollEmployees);
// app.post("/api/addPayroll", payrollController.createPayroll);
// app.post("/api/runPayroll", payrollController.runPayroll);
//
//
// app.get("/api/getEmployeeSupplements/:employeeId", salaryAndSupplementsController.getEmployeeSupplements);
const PORT = 8080;
app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});



