const { format } = require("date-fns");


function modifiedEmployees(employees) {

  const modifiedEmployees = employees.map((employee) => ({
    //Account Details
    id: employee.employee_id,
    first_name: employee.first_name,
    middle_name: employee.middle_name,
    last_name: employee.last_name,
    nickname: employee.nickname,
    citizemship: employee.citizemship,
    identification_number: employee.Identification_number,
    gender: employee.gender,
    other_name: employee.other_name,
    document_type: employee.document_type,
    document_number: employee.document_number,
    date_of_birth: employee.date_of_birth,
    //Contact Details
    address: employee.address,
    street: employee.street,
    state: employee.state,
    city: employee.city,
    municipality: employee.municipality,
    administrative_unit: employee.administrative_unit,
    post_code: employee.post_code,
    email: employee.email,
    contact_number: employee.contact_number,

    //Position Details
    departments: employee.departments.department_name,
    job_positions: employee.job_positions.position_name,
    work_permit: employee.work_permit,
    hire_date: new Date(employee.hire_date),
    registration_date: new Date(employee.registration_date),

    //Contract Details
    national_id: employee.national_id,
    status: employee.status,

  }));

  return modifiedEmployees;
}


module.exports = modifiedEmployees;