const dotenv = require('dotenv');

// Load environment variables from .env.local
dotenv.config();

function getMailOptions(resetToken, email) {
  const mailOptions = {
    from: process.env.MAIL_OPTIONS_FROM,
    to: process.env.MAIL_OPTIONS_TO,// The recipient's email address
    subject: process.env.MAIL_OPTIONS_SUBJECT,
    text: `${process.env.MAIL_OPTIONS_TEXT} ${process.env.RESET_TOKEN_LINK}${resetToken}&email=${email}`,
  };
  return mailOptions;
}

module.exports = getMailOptions;