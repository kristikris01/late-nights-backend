const nodemailer = require('nodemailer');
const dotenv = require('dotenv');

// Load environment variables from .env.local
dotenv.config();
function createTransporter() {
  const transporter = nodemailer.createTransport({
    host: process.env.NODE_MAILER_HOST,
    port: process.env.NODE_MAILER_PORT,
    auth: {
      user: process.env.NODE_MAILER_USER,
      pass: process.env.NODE_MAILER_PASS
    }
  });
  return transporter;
}

module.exports = createTransporter;